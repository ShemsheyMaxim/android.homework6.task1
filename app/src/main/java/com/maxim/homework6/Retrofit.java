package com.maxim.homework6;

import com.maxim.homework6.model.InformationAboutCountry;
import com.maxim.homework6.model.NameCountry;
import com.maxim.homework6.model.Region;
import com.maxim.homework6.model.Subregion;


import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by Максим on 29.05.2017.
 */

public class Retrofit {

    private static final String ENDPOINT = "http://restcountries.eu/rest";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface {
        @GET("/v2/all?fields=region")
//        http://restcountries.eu/rest/v2/all?fields=region;subregion;name
        void getRegion(Callback<List<Region>> callback);

        @GET("/v2/region/{region}?fields=subregion")
        void getSubRegion(@Path("region") String name, Callback<List<Subregion>> callback);

        @GET("/v2/subregion/{subregion}?fields=name")
        void getNameCountry(@Path("subregion") String name, Callback<List<NameCountry>> callback);

        @GET("/v2/name/{name}")
        void getCountries(@Path("name") String name, Callback<List<InformationAboutCountry>> callback);
    }

    private static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getRegion(Callback<List<Region>> callback) {
        apiInterface.getRegion(callback);
    }

    public static void getSubregion(String name, Callback<List<Subregion>> callback) {
        apiInterface.getSubRegion(name, callback);
    }

    public static void getNameCountry(String name, Callback<List<NameCountry>> callback) {
        apiInterface.getNameCountry(name, callback);
    }

    public static void getCountry(String name, Callback<List<InformationAboutCountry>> callback) {
        apiInterface.getCountries(name, callback);
    }
}
