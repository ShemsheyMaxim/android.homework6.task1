package com.maxim.homework6.main_activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.maxim.homework6.sorted.SortedByRegion;
import com.maxim.homework6.R;
import com.maxim.homework6.Retrofit;
import com.maxim.homework6.adapter.RegionAdapter;
import com.maxim.homework6.model.Region;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RegionActivity extends AppCompatActivity implements RegionAdapter.RegionClickListener {
    public static final String KEY = "region";

    private RegionAdapter regionAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_region);

        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.ma_recycler_view);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);


        Retrofit.getRegion(new Callback<List<Region>>() {
            @Override
            public void success(List<Region> regions, Response response) {

                Set<Region> set = new HashSet<>();
                set.addAll(regions);
                List<Region> list = new ArrayList<>();
                for (final Region region : set) {
                    list.add(region);
                }
                Collections.sort(list, new SortedByRegion());
                regionAdapter = new RegionAdapter(RegionActivity.this, (List<Region>) list, RegionActivity.this);
                recyclerView.setAdapter(regionAdapter);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), "Exception with HTTP!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(Region region) {
        Intent intent = new Intent(this, SubregionActivity.class);
        intent.putExtra(KEY, region.getRegion());
        startActivity(intent);
    }
}
