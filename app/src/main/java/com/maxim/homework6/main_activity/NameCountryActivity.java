package com.maxim.homework6.main_activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.maxim.homework6.R;
import com.maxim.homework6.Retrofit;
import com.maxim.homework6.adapter.NameCountryAdapter;
import com.maxim.homework6.model.NameCountry;
import com.maxim.homework6.sorted.SortedByName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class NameCountryActivity extends AppCompatActivity implements NameCountryAdapter.NameCountryClickListener {
    public static final String KEY = "nameCountry";

    private NameCountryAdapter nameCountryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name_country);

        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.ma3_recycler_view);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);

        final String subregionName = getIntent().getStringExtra(SubregionActivity.KEY);

        Retrofit.getNameCountry(subregionName,new Callback<List<NameCountry>>() {
            @Override
            public void success(List<NameCountry> nameCountryList, Response response) {
                Set<NameCountry> set = new HashSet<>();
                set.addAll(nameCountryList);
                List<NameCountry> list = new ArrayList<>();
                for (final NameCountry nameCountry: set){
                    list.add(nameCountry);
                }
                Collections.sort(list,new SortedByName());
                nameCountryAdapter = new NameCountryAdapter(NameCountryActivity.this, list, NameCountryActivity.this);
                recyclerView.setAdapter(nameCountryAdapter);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), "Exception with HTTP!", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onClick(NameCountry nameCountry) {
        Intent intent = new Intent(this, InformationAboutCountryActivity.class);
        intent.putExtra(KEY, nameCountry.getName());
        startActivity(intent);
    }
}
