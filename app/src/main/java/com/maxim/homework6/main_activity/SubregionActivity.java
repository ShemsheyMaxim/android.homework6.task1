package com.maxim.homework6.main_activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.maxim.homework6.R;
import com.maxim.homework6.Retrofit;
import com.maxim.homework6.adapter.SubregionAdapter;
import com.maxim.homework6.model.Subregion;
import com.maxim.homework6.sorted.SortedBySubregion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SubregionActivity extends AppCompatActivity implements SubregionAdapter.SubregionClickListener {
    public static final String KEY = "subregion";

    private SubregionAdapter subregionAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subregion);

        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.ma2_recycler_view);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);

        final String regionName = getIntent().getStringExtra(RegionActivity.KEY);

        Retrofit.getSubregion(regionName, new Callback<List<Subregion>>() {
            @Override
            public void success(List<Subregion> subregions, Response response) {
                Set<Subregion> set = new HashSet<>();
                set.addAll(subregions);
                List<Subregion> list = new ArrayList<>();
                for (final Subregion subregion : set) {
                    list.add(subregion);
                }
                Collections.sort(list, new SortedBySubregion());
                subregionAdapter = new SubregionAdapter(SubregionActivity.this, list, SubregionActivity.this);
                recyclerView.setAdapter(subregionAdapter);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), "Exception with HTTP!", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onClick(Subregion subregion) {
        Intent intent = new Intent(this, NameCountryActivity.class);
        intent.putExtra(KEY, subregion.getSubregion());
        startActivity(intent);
    }
}
