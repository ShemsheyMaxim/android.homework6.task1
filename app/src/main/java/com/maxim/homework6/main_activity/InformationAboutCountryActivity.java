package com.maxim.homework6.main_activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.maxim.homework6.R;
import com.maxim.homework6.Retrofit;
import com.maxim.homework6.model.InformationAboutCountry;
import com.maxim.homework6.model.Language;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class InformationAboutCountryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_about_country);

        final TextView regionTextView = (TextView) findViewById(R.id.ma4_textView_region);
        final TextView subregionTextView = (TextView) findViewById(R.id.ma4_textView_subregion);
        final TextView nameTextView = (TextView) findViewById(R.id.ma4_textView_name);
        final TextView capitalTextView = (TextView) findViewById(R.id.ma4_textView_capital);
        final TextView populationTextView = (TextView) findViewById(R.id.ma4_textView_population);
        final TextView languageTextView = (TextView) findViewById(R.id.ma4_textView_languages);
        final TextView bordersTextView = (TextView) findViewById(R.id.ma4_textView_borders);
        final TextView areaTextView = (TextView) findViewById(R.id.ma4_textView_area);

        final String nameCountry = getIntent().getStringExtra(NameCountryActivity.KEY);

        Retrofit.getCountry(nameCountry, new Callback<List<InformationAboutCountry>>() {

            @Override
            public void success(List<InformationAboutCountry> informationAboutCountriesList, Response response) {
                Set<InformationAboutCountry> set = new HashSet<>();
                set.addAll(informationAboutCountriesList);
                List<InformationAboutCountry> list = new ArrayList<>();
                for (final InformationAboutCountry informationAboutCountry : list) {
                    list.add(informationAboutCountry);
                }
                final InformationAboutCountry informationAboutCountry = informationAboutCountriesList.get(0);

                regionTextView.setText(informationAboutCountry.getRegion());
                subregionTextView.setText(informationAboutCountry.getSubregion());
                nameTextView.setText(informationAboutCountry.getName());
                capitalTextView.setText(informationAboutCountry.getCapital());
                populationTextView.setText(String.valueOf(informationAboutCountry.getPopulation()));

                Language language = informationAboutCountry.getLanguages().get(0);
                String strLanguage = language.getIso639_1() + ", " + language.getIso639_2()
                        + ", " + language.getName() + ", " + language.getNativeName();
                languageTextView.setText(strLanguage);

                String strBorders = "";
                for (int i = informationAboutCountry.getBorders().length - 1; i >= 0; i--) {
                    strBorders = strBorders + informationAboutCountry.getBorders()[i] + " ";
                }
                bordersTextView.setText(strBorders);

                areaTextView.setText(String.valueOf(informationAboutCountry.getArea()));

            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), "Exception with HTTP!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
