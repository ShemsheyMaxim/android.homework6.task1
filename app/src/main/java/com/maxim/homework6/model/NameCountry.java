package com.maxim.homework6.model;

import java.io.Serializable;

/**
 * Created by Максим on 30.05.2017.
 */

public class NameCountry implements Serializable {

    private String name;

    public NameCountry(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
