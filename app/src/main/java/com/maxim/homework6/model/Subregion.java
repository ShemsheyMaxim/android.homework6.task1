package com.maxim.homework6.model;

import java.io.Serializable;

/**
 * Created by Максим on 30.05.2017.
 */

public class Subregion implements Serializable {

    private String subregion;

    public Subregion(String subregion) {
        this.subregion = subregion;
    }

    public String getSubregion() {
        return subregion;
    }

    @Override
    public int hashCode() {
        return subregion.hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        return obj instanceof Subregion && subregion.equals(((Subregion) obj).getSubregion());
    }
}
