package com.maxim.homework6.model;

import java.io.Serializable;

/**
 * Created by Максим on 30.05.2017.
 */

public class Region implements Serializable {

    private String region;

    public Region(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    @Override
    public int hashCode() {
        return region.hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        return obj instanceof Region && region.equals(((Region)obj).getRegion());
    }
}
