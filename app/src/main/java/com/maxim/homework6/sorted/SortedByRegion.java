package com.maxim.homework6.sorted;

import android.support.annotation.NonNull;

import com.maxim.homework6.model.Region;

import java.util.Comparator;

/**
 * Created by Максим on 31.05.2017.
 */

public class SortedByRegion implements Comparator<Region> {

    @Override
    public int compare(Region obj1, Region obj2) {

            String str1 = obj1.getRegion();
            String str2 = obj2.getRegion();

            return str1.compareTo(str2);
        }

}

