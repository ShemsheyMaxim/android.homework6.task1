package com.maxim.homework6.sorted;

import com.maxim.homework6.model.NameCountry;

import java.util.Comparator;

/**
 * Created by Максим on 04.06.2017.
 */

public class SortedByName implements Comparator<NameCountry> {
    @Override
    public int compare(NameCountry obj1, NameCountry obj2) {

        String str1 = obj1.getName();
        String str2 = obj2.getName();

        return str1.compareTo(str2);
    }
}
