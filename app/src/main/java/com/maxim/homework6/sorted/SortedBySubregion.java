package com.maxim.homework6.sorted;

import com.maxim.homework6.model.Subregion;

import java.util.Comparator;

/**
 * Created by Максим on 04.06.2017.
 */

public class SortedBySubregion implements Comparator<Subregion> {
    @Override
    public int compare(Subregion obj1, Subregion obj2) {

        String str1 = obj1.getSubregion();
        String str2 = obj2.getSubregion();

        return str1.compareTo(str2);
    }
}
